<?php
	if(!$_SESSION['active'])
		header("Location: /?page=activate_account")
?>
<div id="add-recipe">	
	<link rel="stylesheet" type="text/css" href="pages/css/add_recipe.css">
	<?php
		if(isset($_GET['id']))
		{
			$id = $_GET['id'];
			echo "<input type='hidden' id='id' value='$id'>";
		}
		if(isset($_GET['edit']))
		{
	?>
	<h1>Edit recipe</h1>
	<?php
		}
		else
		{
	?>
	<h1>Submit new recipe</h1>
	<?php
		}
	?>
	<div id="recipe-name">
		<h3>Recipe Name:</h3>
		<input type="text" id="name-input">
		<div class="recipe-nav">
			<button id="name-next">Next</button>
		</div>
	</div>

	<div id="recipe-description">
		<h3>Description</h3>
		<textarea id="description-input"></textarea>
		<div class="recipe-nav">
			<?php
				if(!isset($_GET['edit']))
				{
			?>	
			<button id="description-back">Back</button>
			<?php
				}
			?>
			<button id="description-next">Next</button>
		</div>
	</div>

	<div id="recipe-image">
		<h3>Choose an image:</h3>
		<p>Upload:</p>
		<form id="imgur">
		  <input type="file" class="imgur" accept="image/*" data-max-size="5000"/>
		</form>
		<img src="" alt="image preview" id="image-preview">

		<p>Search online images:</p>
		<input type="" id="search">
		<div id="search-results">
		</div>

		<div class="recipe-nav">
			<button id="image-back">Back</button>
			<button id="image-next">Next</button>
		</div>
	</div>

	<div id="recipe-ingredients">
		<h3>Add an ingredient</h3>
		<div class="input-ingredient">
			<input type="text" id="add-ingredient-amount-input" placeholder="Units e.g 1/2 cup">
			<input type="text" id="add-ingredient-name-input" placeholder="Ingredient e.g milk">
			<button type="button" id="add-ingredient-button">Add</button>
		</div>
		<p>Currently selected:</p>
		<div id="ingredients">
		</div>
		<div class="recipe-nav">
			<button id="ingredients-back">Back</button>
			<button id="ingredients-next">Next</button>
		</div>
	</div>

	<div id="recipe-steps">
		<h4>Steps</h4>
		<div id="steps">
		</div>
		<button id="add-step-button">Add Step</button><br>
		<div class="recipe-nav">
			<button id="steps-back">Back</button>
			<button id="steps-next">Next</button>
		</div>
	</div>
	<div id="recipe-submit">
		<h2>All done!</h2>
		<p>Press the button below to submit your recipe!</p>
		<button id="submit">Submit</button>
	</div>
</div>
<script src="/pages/js/add_recipe.js"></script>
<?php
	if(isset($_GET['edit']))
		echo "<script>loadExistingData()</script>";
?>