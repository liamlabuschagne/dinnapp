<h1>Home</h1>
<h2>
	Welcome To Dinnapp!
</h2>
<p>
	Search for recipes, add your own, and get a delicious meal every day!
</p>
<nav class="home-nav">
	<?php
		if(isset($_SESSION['username']))
		{
	?>	
	<div class="btn">
		<p class="text">Add Recipe</p>
		<a href="/?page=add_recipe">
			<img src="/assets/images/add.png" width="100px">
		</a>
	</div>

	<div class="btn">
		<p class="text">Recipe Search</p>
		<a href="/?page=recipe_search">
			<img src="/assets/images/search.png" width="100px">
		</a>
	</div>
	<div class="btn">
		<p class="text">Favourites</p>
		<a href="/?page=favourites">
			<img src="/assets/images/favourite.png" width="100px">
		</a>
	</div>
	<div class="btn">
		<p class="text">My Profile</p>
		<a href="/?page=profile">
			<img src="/assets/images/profile.png" width="100px">
		</a>
	</div>
	<div class="btn">
		<p class="text">My Recipes</p>
		<a href="/?page=my_recipes">
			<img src="/assets/images/my_recipes.png" width="100px">
		</a>
	</div>
	<div class="btn">
		<p class="text">Logout</p>
		<a href="?page=logout">
			<img src="/assets/images/logout.png" width="100px">
		</a>
	</div>
</nav>
	<?php
		$message = ["If you can't find a recipe on dinnapp, make sure you add it.","When searching recipes, make sure you use precise keywords and / or ingredients","Go to my Favourites to view recipes you've favourited.","If you need to edit a recipe you added, you can do that on the my recipes page."];
		$message = $message[rand(0,sizeof($message) - 1)];
		echo "<p>Tip: " . $message . "</p>";
		}
		else
		{
	?>
	<div class="btn">
		<p class="text">Recipe Search</p>
		<a href="/?page=recipe_search">
			<img src="/assets/images/search.png" width="100px">
		</a>
	</div>

	<div class="btn">
		<p class="text">Login</p>
		<a href="/?page=login">
			<img src="/assets/images/login.png" width="100px">
		</a>
	</div>

	<div class="btn">
		<p class="text">Register</p>
		<a href="/?page=register">
			<img src="/assets/images/register.png" width="100px">
		</a>
	</div>
</nav>
	<?php
		$message = ["Create an account to access more features, such as favourites, and add your own recipes.","When searching recipes, make sure you use precise keywords and / or ingredients"];
		
		$message = $message[rand(0,sizeof($message) - 1)];
		echo "<p>Tip: " . $message . "</p>";
		}
		
	?>

<?php /*
	if(!isset($_SESSION["mobile"])){
?>
<h2>Get the app</h2>
<p>Dinnapp also has an android app, which lets you get to the recipe you want even faster! <a href="/?page=get_the_app">Click here to get it.</a></p>
<?php
	}*/
?>
