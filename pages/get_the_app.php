<h1>Install the Dinnapp app on android</h1>
<p>Sorry, apple users, we don't have an app for you yet.</p>
<h2>Step 1</h2>
<p>If you aren't on your device, go to this page on your android device and click on the link below:</p>
<p><a href="assets/Dinnapp.apk" download>Download the app installer file</a></p>
<p>This will download the installer.</p>
<h2>Step 2</h2>
<p>Go to your devices settings app.</p>
<p>Tap security, and enable Unknown sources. This allows you to install apps outside of the play store, like this one. Often the play store says you don't have enough room for an app, when you actually do, so even if you can't install anything from the play store you may still be able to install Dinnapp!</p>
<h2>Step 3</h2>
<p>Go to your devices file manager app. Click on downloads, and click on the file Dinnapp.apk you just downloaded.</p>
<p>Click install.</p>
<p>It will install, and you can now use dinnapp from the app!</p>