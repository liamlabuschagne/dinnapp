<?php
	include '../../mysqli_connect.php';
	$id = mysqli_real_escape_string($dbc,$_POST['id']);
	$username = $_SESSION['username'];
	$sql = "SELECT recipe_name, description, items, amounts, steps,creator_name FROM recipes WHERE id = $id ";
	$result = mysqli_query($dbc,$sql);
	
	if(mysqli_num_rows($result) > 0)
	{

		$row = mysqli_fetch_assoc($result);
		if($_SESSION['permissions']== "Moderator" || $_SESSION['permissions']== "Administrator" || $username == $row['creator_name']){
			$items = explode(PHP_EOL,$row['items']);
			$amounts = explode(PHP_EOL,$row['amounts']);
			$steps = explode(PHP_EOL,$row['steps']);

			$row['items'] = $items;
			$row['amounts'] = $amounts;
			$row['steps'] = $steps;
			echo json_encode($row,JSON_FORCE_OBJECT);
		}
	}
	else
		echo false;
?>