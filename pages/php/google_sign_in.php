<?php

require_once '../../google_api/vendor/autoload.php';
include('../../mysqli_connect.php');
// Get $id_token via HTTPS POST.
$id_token = mysqli_real_escape_string($dbc,$_POST['idtoken']);

$CLIENT_ID = '847081259749-t27rko5j08uqs3nbj9dmmc2jlb7b28uf.apps.googleusercontent.com';

$client = new Google_Client(['client_id' => $CLIENT_ID]);
$payload = $client->verifyIdToken($id_token);
if ($payload) {
  
  	// set variables
  	$username = $payload['name'];
  	$user_email = $payload['email'];
  	//id used to identify user
 	$userid = $payload['sub'];
 	$sql = "SELECT id, username, email, google_id, permissions  FROM users WHERE google_id = ". $userid;
 	$result = mysqli_query($dbc, $sql);
 	if(mysqli_num_rows($result)>0){
  		while($row = mysqli_fetch_row($result))
		{
			//update profile info
			if($row[1] != $username){
				$update_sql = 'UPDATE users SET email = "' . $username . ' " WHERE google_id =' . $userid;
				mysqli_query($dbc, $update_sql);
			}
			if($row[2] != $user_email){
				$update_sql = 'UPDATE users SET email = "' . $user_email . ' " WHERE google_id =' . $userid;
				mysqli_query($dbc, $update_sql);
			}
			$permissions = $row[4];
			$id = $row[0];
		}
	}else {
		// insert data to mysql if new user
		$insert_sql = 'INSERT INTO users (username,email,google_id,permissions,active) VALUES ("'.$username.'","'. $user_email .'","' . $userid .'","User",1)';
		mysqli_query($dbc, $insert_sql);
		$permissions = "User";
		$request_sql = "SELECT id FROM users where google_id == " . $userid;
		$id = mysqli_fetch_row(mysqli_query($dbc, $request_sql))[0];
	}
	$_SESSION['username'] = $username;
	$_SESSION['permissions'] = $permissions;
	$_SESSION['id'] = $id;
	$_SESSION['active'] = true;
	//header("Location: /");
} else {
  // Invalid ID token
}


?>