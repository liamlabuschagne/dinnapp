<?php
	include '../../mysqli_connect.php';
	$recipe_id = mysqli_real_escape_string($dbc,$_POST['recipe_id']);
	$rating = mysqli_real_escape_string($dbc,$_POST['rating']);
	if($rating > 10) $rating = 10;
	$title = mysqli_real_escape_string($dbc,$_POST['title']);
	$comment = mysqli_real_escape_string($dbc,$_POST['comment']);

	// Get users id
	$username = $_SESSION['username'];
	$user_id = $_SESSION['id'];
	$date = getdate();
	$date = $date['year'] ."-". $date['mon'] ."-". $date['mday'];
	// Check if they have already added a rating
	$sql = "SELECT rating FROM ratings WHERE user_name = '$username' AND user_id = '$user_id'";
	// update recipes table
	$result = mysqli_query($dbc,$sql);
	// if there was a rating
	if(mysqli_num_rows($result) > 0){
		$old_rating = mysqli_fetch_row($result)[0];
		// get data from recipes table
		$sql = "SELECT avg_rating, num_of_ratings FROM recipes WHERE id = $recipe_id";
		$result = mysqli_query($dbc,$sql);
		$row = mysqli_fetch_row($result);
		$avg_rating = $row[0];
		$num_of_ratings = $row[1];
		// expand ratings
		$expanded_ratings = $avg_rating * $num_of_ratings;
		// remove old rating
		$expanded_ratings = $expanded_ratings - $old_rating;
		// add new rating
		$expanded_ratings = $expanded_ratings + $rating;
		// shrink back down and update
		$avg_rating = $expanded_ratings / $num_of_ratings;
		$sql = "UPDATE recipes SET avg_rating = $avg_rating WHERE id = $recipe_id";
		mysqli_query($dbc,$sql);
	} else {
		// if first rating user has created for this recipe
		// get data from recipes table - to update its avg rating
		$sql = "SELECT avg_rating, num_of_ratings FROM recipes WHERE id = $recipe_id";
		$result = mysqli_query($dbc,$sql);
		$row = mysqli_fetch_row($result);
		$avg_rating = $row[0];
		$num_of_ratings = $row[1];
		// expand ratings
		$expanded_ratings = $avg_rating * $num_of_ratings;
		// add new rating, add one to number of ratings for new rating
		$expanded_ratings = $expanded_ratings + $rating;
		$num_of_ratings = $num_of_ratings + 1;
		// shrink back down and update
		$avg_rating = $expanded_ratings / $num_of_ratings;
		$sql = "UPDATE recipes SET avg_rating = $avg_rating, num_of_ratings = $num_of_ratings WHERE id = $recipe_id";
		mysqli_query($dbc,$sql);
	}
	// delete old rating if existed
	$sql = "DELETE FROM ratings WHERE user_name = '$username' AND user_id = $user_id";
	// update recipes table
	$result = mysqli_query($dbc,$sql);
	// Insert rating into db
	$sql = "INSERT INTO ratings (recipe_id, user_id, user_name, rating, title, comment, date ) VALUES ($recipe_id, $user_id, '$username' ,$rating, '$title', '$comment', '$date')";
	$result = mysqli_query($dbc,$sql);
	echo $result;
	
	
?>