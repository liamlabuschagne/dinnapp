<?php
	if(isset($_SESSION['username']))
	{
?>
<div id="contact">
	<h1>Contact</h1>
	<p>To contact us please type your information below and a message and we will get back to you as soon as possible.</p>
	<label>Message:<label><br /><textarea id="message"></textarea>
	<br />
	<button id="submit">Submit</button>
</div>
<?php
	}
	else
	{
?>
<h1>Contact</h1>
<p>Please sign in to contact us or contact us through our email: <a href="mailto:info@dinnapp.tk">info@dinnapp.tk</a></p>
<?php
	}
?>
<script src="/pages/js/contact.js"></script>