<?php
	include '../../mysqli_connect.php';
	$recipe_id = mysqli_real_escape_string($dbc,$_POST['recipe_id']);
	$user_id = $_SESSION['id'];
	// check if they have it as a favorite
	$sql = "SELECT id FROM favourites WHERE user_id = $user_id AND recipe_id = $recipe_id";
	$result = mysqli_query($dbc,$sql);
	if(mysqli_num_rows($result) > 0){
		// if it has been favourited delete
		$sql = "DELETE FROM favourites WHERE user_id = $user_id AND recipe_id = $recipe_id";
		$result = mysqli_query($dbc,$sql);
		if ($result){
			echo "removed";
		}
		
	} else {
		// if it hasnt been favourited add
		$sql = "INSERT INTO favourites (user_id, recipe_id) VALUES ($user_id, $recipe_id)";
		$result = mysqli_query($dbc,$sql);
		if ($result){
			echo "added";
		}
		
	}
?>