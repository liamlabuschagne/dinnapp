<?php
	/* 
		This array holds all the specific meta data for each page on the website.
		Please add an entry to this array if adding new pages.

		The meta array contain associative arrays, these arrays have the properties:
		title and description.

		When adding a new page use this format.
		'[file name (no .php included)]' => array(
			'title' => '[Title for the page here]',
			'description' => '[Description for the page here]'
		), 
	*/  
    $metas = array(

    	'home' => array(
			'title' => 'Dinnapp - Home',
			'description' => 'Dinnapp saves time and money by choosing dinners based on what you already have in your home, and then shows you a list of great recipies!'
		),

		'about' => array(
			'title' => 'Dinnapp - About',
			'description' => 'Learn all about how Dinnapp works and how it came to be!'
		),

		'contact' => array(
			'title' => 'Dinapp - Contact',
			'description' => 'Please let us know of any comments, questions or concerns, we are happy to help!'
		),
		'login' => array(
			'title' => 'Dinnapp - Login',
			'description' => 'Login to your account.' 
		),
		'register' => array(
			'title' => 'Dinnapp - Register',
			'description' => 'Create your free account to unlock great features.' 
		),
		'add_recipe' => array(
			'title' => 'Dinapp - Add Recipe',
			'description' => 'Add your favorite recipe to Dinnapp now!'
		),
		'recipe_search' => array(
			'title' => 'Dinapp - Search recipes',
			'description' => 'Search our collection of great recipes.'
		),
		'my_recipes' => array(
			'title' => 'Dinnapp - My Recipes',
			'description' => 'View all of your recipes.'
		),
		'profile' => array(
			'title' => 'Dinnapp - My Profile',
			'description' => 'View your profile information and account settings.' 
		),
		'get_the_app' => array(
			'title' => 'Dinnapp - Get the App',
			'description' => 'Download page for the Dinnapp app, with installation instructions' 
		),
		'favourites' => array(
			'title' => 'Dinnapp - My Favourites',
			'description' => 'Quickly get to all the recipes you have favourited.' 
		)
    );

    // This include a pure html file containing meta data which is needed on all pages.
    include 'baseHead.html';

    /* 
    	This will insert page specific meta data using the 'page' URL get variable to index the 'metas' array, which contains a title and description for each page. 
	*/
    if(isset($_GET['page']) && isset($metas[($_GET['page'])]) )
    {
    	$page = $_GET['page'];
    	echo "\t<title>".$metas[$page]['title']."</title>\n";
    	echo "\t<meta name='description' content='".$metas[$page]['description']."' />\n";
    }
    else // By default use the 'home' page meta data
    {
    	echo "\t<title>".$metas['home']['title']."</title>\n";
    	echo "\t<meta name='description' content='".$metas['home']['description']."' />\n";
    }
?>