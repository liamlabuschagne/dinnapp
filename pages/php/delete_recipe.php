<?php
	if(isset($_SESSION['username']) && isset($_GET['id']))
	{
		$id = mysqli_real_escape_string($dbc,$_GET['id']);
		$username = mysqli_real_escape_string($dbc,$_SESSION['username']);
		// Check that the user owns this recipe and delete it at the same time
		$sql = "DELETE FROM recipes WHERE id = $id AND creator_name = '$username'";
		$result = mysqli_query($dbc, $sql);
		if($result)
			header('Location: /?page=my_recipes');
		else
			echo "Failed to delete";
	}
?>