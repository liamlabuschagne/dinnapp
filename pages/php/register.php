<?php
	include "../../mysqli_connect.php";
	// Create variables to store all the data and strip them.
	$username = mysqli_real_escape_string($dbc,$_POST['username']);
	$email = mysqli_real_escape_string($dbc,$_POST['email']);
	$password = mysqli_real_escape_string($dbc,$_POST['password']);

	// Create hashed version of the password.
	$password_hashed = password_hash($password,PASSWORD_DEFAULT);

	// Create a uniqe hash to be used for email verifiction
	$hash = uniqid();

	// Submit their information to the database
	$sql = "INSERT INTO users
			(username,password,email,hash,permissions,google_id)
			VALUES
			('$username','$password_hashed','$email','$hash','User',-1)";
	
	$successful = mysqli_query($dbc,$sql);
	
	if($successful)
	{
		// Log the user in
		$result = mysqli_query($dbc,"SELECT id FROM users WHERE email = '$email' AND password = '$password_hashed'");
		$_SESSION['username'] = $username;
		$_SESSION['id'] = mysqli_fetch_row($result)[0];
		// Tell ajax everything went smoothly
		$subject = "Account Verifiction";
		$txt = "Hi there! Thanks for registering for Dinnapp.tk! To let us know your email works please click this link: http://www.dinnapp.tk/?page=verify_email&email=$email&hash=$hash";
		$headers = "From: info@dinnapp.tk" . "\r\n";
		mail($email,$subject,$txt,$headers);
		echo true;
	}
	else
	{
		echo false;
	}
?>