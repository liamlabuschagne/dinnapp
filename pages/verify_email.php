<?php
	
	function error()
	{
		echo "Sorry, your email could not be verifed, please try following the instructions sent to you 
				through email again or <a href='/?page=resend_email'>click here</a> to resend it.";
	}

	// Check they have provided the correct data
	if(
		isset($_GET['email']) && !empty($_GET['email']) &&
		isset($_GET['hash']) && !empty($_GET['hash'])
	)
	{
		// Store data in variables
		$email = $_GET['email'];
		$hash = $_GET['hash'];

		// Get the hash associated with the given email from the database
		$sql = "SELECT hash FROM users WHERE email = '$email'";
		$result = mysqli_query($dbc,$sql);

		// Check if the email exists
		if($result)
		{
			// Store the hash retreved from the database in a variable
			$db_hash = mysqli_fetch_row($result)[0];

			// Check to see if the hash provided by the user is correct
			if($hash == $db_hash)
			{
				// Active the user
				$sql = "UPDATE users SET active = 1 WHERE hash = '$hash'";
				$result = mysqli_query($dbc,$sql);
				if($result)
				{
					// Tell the user they have been activated
					echo "Account activated!";
				}
				else
				{
					error();
				}
			}
			else
			{
				error();
			}
		}
		else
		{
			error();
		}
	}
	else
	{
		echo "<h1>How to active your account</h1>";
		echo "<p>Please check the email you provided, for a verification email and follow the instructions there.
		If you haven't recieved an email please <a href='/?page=resend_email'>click here</a> to resend it.</p>";
	}
?>