var page = 1
$(document).ready(() =>{
	// script for normal search
	$("#input").on('keyup', () => {
		if($('#input').val() == ""){
			$('#results').html("");
			return;
		}
		$.post("/pages/php/recipe_search.php", {searchString: $('#input').val()}, function(data) {
			page = 1
			$('#results').html(data);
			$("#prev-button").prop('disabled',true)
			if($("#num-rows").val() == 10){
				$("#next-button").prop('disabled',false)
			} else {
				$("#next-button").prop('disabled',true)
			}
		});
	});
	// script for random items search
	$("#random_results").click( () => {
		$.post("/pages/php/recipe_search.php", {random: ""}, function(data){
			$('#results').html(data);
			// disable both next and previous buttons
			$("#prev-button").prop('disabled',true)
			$("#next-button").prop('disabled',true)
		});
	});
	// next button clicked
	$("#next-button").click(() =>{
		if($("#num-rows").val() == 10){
			page++
			var offset = (page-1) * 10
			$.post("/pages/php/recipe_search.php", {searchString: $('#input').val(),offset: offset }, function(data) {
				$('#results').html(data);
				// update buttons
				$("#prev-button").prop('disabled',false)
				if($("#num-rows").val == 10){
					$("#next-button").prop('disabled',false)
				} else {
					$("#next-button").prop('disabled',true)
				}
			})
		}
		
	})
	// previous button clicked
	$("#prev-button").click(() =>{
		
		page--
		var offset = (page-1) * 10
		$.post("/pages/php/recipe_search.php", {searchString: $('#input').val(),offset: offset }, function(data) {
			$('#results').html(data);
			// update button values
			if(page < 2){
				$("#prev-button").prop('disabled',true)
			} else {
				$("#prev-button").prop('disabled',true)
			}
			$("#next-button").prop('disabled',false)
			
		})
		
	})

});
// autofilling next word in when clicked on and re searching
function autofill(value){
	$('#selected-ingredients').append("<li>"+$('#key_item0').html()+"</li>");
	$('#input_ingredients').val("");
	$('#results_ingredients').html("");
}