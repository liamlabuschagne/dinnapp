function onSignIn(googleUser) {
  	var id_token = googleUser.getAuthResponse().id_token;
  	$.post('/pages/php/google_sign_in.php',
  		{
  			idtoken:  id_token 
  		}
  		,function (data,state){
  			console.log(data);
  			console.log(state);
        location.replace('/')
  		}
    );
}
