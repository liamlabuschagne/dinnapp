$(document).ready(()=>{
	// favorite button
	$('.favourite-btn').on("click",() =>{
		$.post('/pages/php/add_favourite.php',{recipe_id:$('#recipe-id').val()},(data)=>
			{
				if(data == "added"){
					$('#favourite-icon').attr("src","../../assets/images/favourited.png")
					$("#favourites-message").html("Remove from favourites")
				}
				if(data=="removed"){
					$('#favourite-icon').attr("src","../../assets/images/favourite.png")
					$("#favourites-message").html("Add to favourites")
				}
			})
		
	})
	// ratings script
	$('#submit').on('click', ()=>{
		$.post('/pages/php/add_rating.php', {recipe_id: $('#recipe-id').val() ,rating: $('#rating').val(), title: $('#title').val(), comment: $('#comment').val()}, (data)=>{
			if(data)
			{
				$('.create-rating').html("<p>Successfully added your rating!</p>");
				$.post('/pages/php/get_ratings.php',{recipe_id: $('#recipe-id').val()}, (data)=>{
					$('#ratings').html(data);
				});
			}
			else
			{ 
				$('#rate-recipe').html("Sorry! Couldn't add that rating!");
			}
		});
	});
	$.post('/pages/php/get_ratings.php',{recipe_id: $('#recipe-id').val()}, (data)=>{
		$('#ratings').html(data);
	});
});