$(document).ready(()=>{
	$('#btn').hide();
	$("#checkbox").on('click', () =>{
		$('#btn').toggle();
	});

	$('#btn').on('click', ()=>{
		$.post('/pages/php/delete_account.php', (data) =>{
			if(data == "Successful")
			{
				window.location.href="/";
			}
			else
				$('#delete-account').html("<h1>Sorry! We couldn't delete your account! Please try again.</h1>");
		});
	});
});
