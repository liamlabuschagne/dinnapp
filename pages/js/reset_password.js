$(document).ready(()=>{
	$('#btn').on('click', () => {
		if($('#password').val() != $('#password-repeat').val())
		{
			$('#errors').html("<p>Passwords dont match.<p>");
			return;
		}
		$('#errors').html($('#password').val() + $('#password-repeat').val());
		if($('#password').val().length < 6)
		{
			$('#errors').html("<p>Please choose a longer password.</p>");
			return;
		}
		$.post('/pages/php/reset_password.php', {email: $('#email').val(), hash: $('#hash').val(), password: $('#password').val()} ,(data) =>{
			if(data)
			{
				$('#reset-password').html("<p>Password Reset!</p>");
			}
			else
			{
				$('#reset-password').html('<p>Oops! We could not reset your password, please try again.');
			}
		});
	});
});