function check_username()
{
	$.post('/pages/php/check_username.php', {username: $('#username').val()}, (data) =>{
		if(data == "false")
		{
			$('#errors').append("Username taken.<br>");
			error = true;
		}
		check_email();
	});
}

function check_email()
{
	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	var email_valid = true;
	// Check email is valid
	if(!re.test($('#email').val()))
	{
		$('#errors').append("Email is not valid.<br>");
		error = true;
		email_valid = false;
		check_passwords();
	}
	if(email_valid)
	{
		$.post('/pages/php/check_email.php', {email: $('#email').val()}, (data) =>{
			if(data == "false")
			{
				$('#errors').append("Email has already been used.<br>");
				error = true;
			}
			check_passwords();
		});
	}
}

function check_passwords()
{
	if($('#password').val().length < 6)
	{
		error = true;
		$('#errors').append("Please choose a longer password.<br>");
	}

	// Check if passwords match
	if($('#password').val() != $('#password-repeat').val())
	{
		$('#errors').append("Password and password repeat don't match.<br>");
			error = true;
	}
	send_data();
}

function send_data()
{
	if(!error)
	{
		$.post(
		'/pages/php/register.php',
		{
			username: $('#username').val(),
			email: $('#email').val(),
			password: $('#password').val()
		},
		(success) => {
			if(success)
			{
				window.location.href="/";

			}
			else
			{
				$('#errors').html("Could not register account!");
			}
		} 
		);
	}
}

function run_checks()
{
	check_username();
}

let error = false;

$(document).ready( () => {
	// When the user clicks submit:
	$('#submit').on('click', () => {
		$('#errors').html("");
		error = false;
		// Check that all the feilds have been entered
		if(
		   !(
		   $('#username').val() != "" && $('#email').val() != "" 
		   && $('#password').val() != "" && $('#password-repeat').val()
		   )	
		)
		return;	

		run_checks();
	});

});