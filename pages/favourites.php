<?php
	if(!$_SESSION['active'])
		header("Location: /?page=activate_account")
?>
<h1>My Favourites</h1>
<?php
	
	if(isset($_SESSION['id'])){
		$user_id = $_SESSION['id'];
		// if unfavourite is set remove that from the database
		if(isset($_GET["unfavourite"])){
			$unfavourite_id = mysqli_real_escape_string($dbc,$_GET["unfavourite"]);

			$sql = "DELETE FROM favourites WHERE user_id = $user_id AND recipe_id = $unfavourite_id";
			mysqli_query($dbc,$sql);
		}
		// Get all of the user's favourite recipe ids:
		$sql = "SELECT recipe_id FROM favourites WHERE user_id = $user_id";
		$result = mysqli_query($dbc,$sql);
		if (mysqli_num_rows($result) > 0){
			$favourited_recipes = [];
			while($row = mysqli_fetch_row($result)){
				array_push($favourited_recipes, $row[0]);
			}
			// get all the recipes
			$sql = "SELECT id,recipe_name,description FROM recipes WHERE id = " . $favourited_recipes[0];
			for($i = 1 ; $i < count($favourited_recipes) ; $i++){
				$sql .= " OR id = " . $favourited_recipes[$i];
			}
			$result = mysqli_query($dbc, $sql);
			// fetch data
			while($row = mysqli_fetch_row($result))
			{
				$id = $row[0];
				echo "<div class='recipe'>";
				echo "<a href='/?page=view_recipe&id=$id'><h2>"
				.$row[1]."</h2></a>";
				echo "<a class='unfavourite-btn' href='/?page=favourites&unfavourite=$id'><img width='30px' src='/assets/images/favourited.png' /></a>";
				// echo first part of description
				echo "<p>" . substr($row[2],0,200) . "...</p>";
				echo "</div>";
			}
		}
		if(mysqli_num_rows($result) < 1)
		{
			echo "<p>You don't have any favourite recipes yet.</p>";
		}
	}
	?>
<script src="/pages/js/favourites.js"></script>