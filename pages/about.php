<h1>About Dinnapp</h1>
<h2>Purpose</h2>
<p>
This website's purpose is to make recipes for all kinds of dishes and baking, avalible to anyone in the world. This website allows you to submit recipes in an easy an intuative way. Once submitted, recipes can be shared to anyone. Our website also incorperates our very own search algorithm which can find recipes that best match the keywords you provide.
</p>
<h2>Where's the ads?</h2>
<p>
Everyone hates ads so we have decided to keep this website 100% free of ads or other anoying things found on most other sites. You will never find yourself closing down popups or declining email newsletters. 
</p>
<h2>The creation of Dinnapp</h2>
<p>
This website was created in 2017 by two high school students: Liam Labuschagne and Bert Downs. We created this website to test our skills in web development and also to test our collaboration skills. This is the first project we have ever collaborated on.
</p>