<div id="delete-account">
	<h1>Delete Account</h1>
	<p>Deleting your account is not reversable. All your recipes will be deleted along with any comments/ratings you have
	submitted on other people's recipes.</p>
	<h3>Are you sure?</h3>
	<input type="checkbox" id="checkbox"><label>Yes, I'm sure.</label>
	<button id="btn">Delete my account</button>
	<script src="/pages/js/delete_account.js"></script>
</div>