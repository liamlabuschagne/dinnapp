<?php
	if( isset($_SESSION['username']) &&
		isset($_GET['ct']) && 
		isset($_GET['cid'])&&
		isset($_GET['ruid']))
	{
		$ct = $_GET['ct'];
		$cid = $_GET['cid'];
		$ruid = $_GET['ruid'];
		$username = $_SESSION['username'];
		// Get the id of the reporter
		$sql = "SELECT id FROM users WHERE username = '$username'";
		$rid = mysqli_fetch_row(mysqli_query($dbc,$sql))[0];
		echo "<div id='report'>\n";
		echo "<input type='hidden' value='$ct' id='ct'>\n";
		echo "<input type='hidden' value='$cid' id='cid'>\n";
		echo "<input type='hidden' value='$ruid' id='ruid'>\n";
		echo "<input type='hidden' value='$rid' id='rid'>\n";
	}
	else
	{
		header('Location: /');
	}
?>
<h1>Report</h1>
<p>Thanks for taking the time to report inappropriate or useless ratings/recipes! Please fill in the information below to help us understand why you are reporting this content.</p>
<label>Reason for reporting</label><br>
<textarea id="reason"></textarea><br>
<button id="submit">Report</button>
<script src="/pages/js/report.js"></script>
</div>