<link rel="stylesheet" href="/pages/css/view_recipe.css">
<?php
	if(!isset($_GET['id']))
		header("Location: /");
	$id = mysqli_real_escape_string($dbc,$_GET['id']);
	$sql = "SELECT recipe_name, creator_name, description, items, amounts, steps, avg_rating, num_of_ratings, creator_id
			FROM recipes WHERE id = '$id'";
	$result = mysqli_query($dbc,$sql);
	$recipe_found = false;
	if(mysqli_num_rows($result) > 0)
	{
		$recipe_found = true;
		while($row = mysqli_fetch_row($result))
		{
			$creator_id = $row[8];
			echo "<div class='view-recipe'>";
			echo "<div class='recipe-header'>";
			echo "<h1 class='recipe-name'>". $row[0] ."</h1>";
			echo "<p class='creator-name'>Created by: <a href='/?page=my_recipes&id=$creator_id'>". $row[1] ."</a></p>";
			$rating = round($row[6]);
			echo "<p class='rating'>Rated: ". $rating ."</p></div>";
			echo "<div class='description'><h2>Description</h2>";
			echo "<p>". $row[2] . "</p></div>";
			echo "<div class='ingredients'><h2>Ingredients</h2>";
			$items = explode(PHP_EOL, $row[3]);
			$amounts = explode(PHP_EOL, $row[4]);

			foreach ($items as $key => $item) {
				echo "<div><p class='ingredient'>".$amounts[$key]." ".$item."</p></div>";
			}
			// Maybe impliment a custom mark down for how to layout the steps?
			echo "</div>";
			echo "<div class='steps'><h2>Steps:</h2>";
			$steps = explode(PHP_EOL, $row[5]);
			foreach ($steps as $key => $value) {
				echo "<div class='step'><label class='step-number'>Step ".($key+1)."</label><br>";
				echo "<p class='step-content'>". $value . "</p></div>";
			}
			echo "</div><div class='links'><a href='/?page=report&ct=Recipe&cid=".$id."&ruid=".$row[8]."' target='_blank'>Report this recipe</a><br>";
		}
	}
	else
	{
		echo "<p>Sorry! We couldn't find that recipe, it may have been deleted.</p>";
	}
	if($recipe_found)
	{
		$url = "http://www.dinnapp.tk/?page=view_recipe&id=".$id;
		$url = urlencode($url);
?>
<a href="http://facebook.com/share.php?u=<?php echo $url; ?>" target="_blank"><img src="/assets/images/facebook.png" width="100px"></a><br>
<?php
	echo "<input type='hidden' id='recipe-id' value='$id'>";
	if (isset($_SESSION['permissions'])){
		if($_SESSION['permissions'] == "Administrator" || $_SESSION['permissions'] == "Moderator"){
			echo "<a href='/?page=add_recipe&edit=1&id=". $id ."'>Change recipe</a><br>";
			echo "<a href='/admin?page=block_account&id=". $creator_id ."'>block user</a><br>";
		}

	}
	if(isset($_SESSION['username']))
	{
?>
<a href='#favourite-btn' >
	<div class='favourite-btn'>
		<?php
		if(isset($_SESSION['id']) && $_SESSION['active']){
			$user = $_SESSION['id'];
			$sql = "SELECT id FROM favourites WHERE recipe_id = $id AND user_id = $user";
			$result = mysqli_query($dbc,$sql);
			if (mysqli_num_rows($result) > 0){
				echo '<img width="30px" id="favourite-icon" src="/assets/images/favourited.png">';
				echo '<p id="favourites-message">Remove from favourites</p>';
			} else {
				echo '<img width="30px" id="favourite-icon" src="/assets/images/favourite.png">';
				echo '<p id="favourites-message">Add to favourites</p>';
			}
		}
		if(!$_SESSION['blocked']){
		?>
		
	</div>
</a>
</div>
<div class="create-rating">
	<h1>Rate this recipe</h1>
	<p>(Note: If you have already rated this recipe, rating it again will delete the old rating.)</p><br />
	<label>Rating:</label><input type="number" min="1" max="10" value="1" id="rating" oninput="validity.valid||(value='');"><span>/10</span>
	<h2>Comment: (optional)</h2>
	<label>Comment title:</label><input type="text" id="title">
	<br />
	<label>Comment message:</label>
	<br />
	<textarea id="comment"></textarea>
	<br />
	<button id="submit">Rate</button>

	<?php
			}
		}
		else
		{
			if(!isset($_SESSION['username']))
				echo "<p>Please <a href='/?page=login'>login</a> to add a rating.</p>";
			else if(!$_SESSION['active'])
				echo "<p>Please activate your account with your email address to add ratings.</p>";
		}
	?>
</div>
<div id="ratings">
</div>
<?php
	}
?>
<script src="/pages/js/view_recipe.js"></script>