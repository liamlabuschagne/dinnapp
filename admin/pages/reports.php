<h1>Reports</h1>
<?php
	
	// Get the reports
	$sql = "SELECT * FROM reports";
	$result = mysqli_query($dbc,$sql);
	if($result && mysqli_num_rows($result) > 0)
	{
		echo "<table cellspacing='20'>";
		echo "<tr><th>Content Type</th><th>Content ID</th><th>Reported user ID</th><th>Reason</th><th>Reported Content</th><th>Delete content</th><th>Delete report</th>";
		while($row = mysqli_fetch_assoc($result))
		{
			echo "<tr>";
			echo "<td>".$row['content_type']."</td>";
			echo "<td>".$row['reported_content_id']."</td>";
			echo "<td>".$row['reported_user_id']."</td>";
			echo "<td>".$row['reason']."</td>";

			// Get reported content
			$reported_content_id = $row['reported_content_id'];

			if($row['content_type'] == "Rating"){
				$sql = "SELECT rating,title,comment FROM ratings WHERE id = '$reported_content_id'";
			}
			else if($row['content_type'] == "Recipe"){
				$sql = "SELECT description,items,amounts,steps FROM recipes WHERE id = '$reported_content_id'";
			}

			$getContentRes = mysqli_query($dbc,$sql);

			if($row['content_type'] == "Rating")
			{
				$rating = mysqli_fetch_row($getContentRes);
				$content = $rating[0] ." ". $rating[1] ." ". $rating[2]; 
			}
			else if($row['content_type'] == "Recipe")
			{
				$recipe = mysqli_fetch_row($getContentRes);
				$content = $recipe[0] . "<br />" . $recipe[1] . "<br />" . $recipe[2] . "<br />" . $recipe[3]; 
			}
			// print table row on screen
			echo "<td>".$content."</td>";
			if($row['content_type']=="Recipe")
				$ct = "recipes";
			else
				$ct = "ratings";
			echo "<td><a href='/admin/pages/php/delete.php?&ct=".$ct."&rid=".$row['id']."&cid="
			.$reported_content_id."'>Delete content</a><br><a href='/admin/pages/php/delete.php?&ct=recipes&cid=".$row['reported_content_id']."&rid=".$row['id']."'>Change recipe</a><br></td>";
			echo "<td><a href='/admin/pages/php/delete.php?&ct=reports&cid=".$row['id']."'>Delete report</a></td>";
			echo "</tr>";
		}
		echo "</table>";
	}
	else
		echo "<h3>No reports</h3>";
?>