$(document).ready( () => {
	$('#submit').on('click',()=>{
		$('#errors').html("");
		if($('#username').val() != "" && $('#password').val() != "")
		{
			$.post('/admin/pages/php/login.php', {username: $('#username').val(), password: $('#password').val()}, 
				(data) => {
					if(data)
						window.location.href=".";
					else
						$('#errors').html("Incorrect username or password");
			});
		}
	});
});