<?php
	include '../mysqli_connect.php';
?>
<!DOCTYPE html>
<html>
<head>
	<title>Admin - Login</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<body>
	<main>
		<?php
			// Check the URL get variables for 'page', if that page exits include it. 
			if(isset($_GET["page"]) && file_exists("pages/" . $_GET["page"] . ".php")){
				include "pages/" . $_GET["page"] . ".php";

			} else {
				include "pages/login.php";
			}
		?>
	</main>
</body>
</html>
