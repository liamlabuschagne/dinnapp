# Dinnapp dinner chooser
This is a dinner chooser application. Created by Liam Labuschagne and Bert Downs
## Features:
* Get the app to choose random dinners
* Choose dinners based on contents of your kitchen
* Rate dinners based on how easy it is to cook, cost and taste
* Easily add new recipies
* Cost: Free! (Everyone loves free!)

## DB Info
* URL: databases.000webhost.com
* Username: id3563234_dinnapp
* Password: CLASSIFIED