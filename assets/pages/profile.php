<?php
	// Check that the user is logged in
	if(isset($_SESSION['username']) && !empty($_SESSION['username']))
	{
		//Store the user's username from session
		$username = $_SESSION['username'];
		// Get the user's information
		$sql = "SELECT active,email,hash,google_id FROM users WHERE username = '$username'";
		$result = mysqli_query($dbc,$sql);
		
		if($result)
		{
			$active = 'no';
			$email = '';
			$hash = '';
			$google_id = 0;
			while($row = mysqli_fetch_assoc($result))
			{
				$email = $row['email'];
				$active = $row['active'];
				$hash = $row['hash'];
				$google_id = $row['google_id'];
			}

			// Show the user ther info
			echo "<h1>Your Profile</h1>";
			echo "<h2>Username: ".$username."</h2>";
			echo "<h2>Email: ".$email."</h2>";

			// Check wether the user is active
			if($active && $google_id == -1)
			{
				echo "<h2>Account active</h2>";
			}
			else if(!$active)
			{
				echo "<h2>Account not active</h2>";
				echo "<a href='/?page=verify_email'>Please activate your account.</a><br>";
			}

			if($google_id == -1)
				echo "<a href='/?page=reset_password&email=$email&hash=$hash'>Change Password</a><br />";

			echo "<a href='/?page=delete_account'>Delete account</a>";
		}
		else
		{
			// Tell the user that their profile could not be loaded
			echo "Sorry, we couldn't load your profile, please try again.";
		}
	}
	else
	{
		// Redirect the user to the home page.
		header("Location: /");
	}
?>