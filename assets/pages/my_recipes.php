<?php
	if(!$_SESSION['active'])
		header("Location: /?page=activate_account")
?>
<link rel="sylesheet" href="/pages/css/my_recipes.css">
<h1>My Recipes</h1>
<?php
	// Get all of the user's recipes
	$username = $_SESSION['username'];
	$sql = "SELECT id,recipe_name,description FROM recipes WHERE creator_name = '$username'";
	$result = mysqli_query($dbc, $sql);
	// fetch data
	while($row = mysqli_fetch_row($result))
	{
		$id = $row[0];
		echo "<div class='recipe'>";
		echo "<a class='recipe-edit-btn' href='/?page=add_recipe&edit=1&id=".$row[0]."'><img width='30px' src='/assets/images/edit.png' /></a>";
		echo " <a class='recipe-delete-btn' href='/?page=delete_recipe&id=".$row[0]."'><img width='30px' src='/assets/images/delete.png' /></a>";
		echo "<a style='padding:0' href='/?page=view_recipe&id=".$id."'><h2>"
		.$row[1]."</h2>";
		// echo first part of description
		echo "<p>" . substr($row[2],0,100) . "...</p></a>";
		echo "</div>";
	}

	if(mysqli_num_rows($result) < 1)
	{
		echo "<p>You don't have any recipes yet.</p>";
	}
?>