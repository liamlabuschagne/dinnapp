<?php
	//normal/session sign out
	if(isset($_SESSION["mobile"])){
		// if in mobile version
		session_unset();
		// carry over mobile session variable
		$_SESSION["mobile"] = true;
	} else{
		session_destroy();
	}
    header("Location:/")
?>
