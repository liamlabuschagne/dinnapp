let ingredientCount = 0;
let edit = false;
function loadExistingData()
{
	$.post('/pages/php/get_recipe.php', {id: $('#id').val()}, (data)=>{
		if(!data){
			$('#add-recipe').html("Sorry! Couldn't find that recipe!");
			edit = false;
			return;
		}
		else
			edit = true;
		let json = JSON.parse(data);
		$('#name-input').val(json.recipe_name);
		$('#description-input').val(json.description);
		let len = 0;
		let i = 0;
		while(json.items[i])
		{
			++i;
			++len;
		}
		for(let i = 0; i < len; ++i)
		{
			addIngredient(json.amounts[i],json.items[i]);
		}
		len = 0;
		i = 0;
		while(json.steps[i])
		{
			++i;
			++len;
		}
		for(let i = 0; i < len; ++i)
		{
			addStepWithContent(json.steps[i]);
		}
	});
}

function addIngredient(amount,name) {
	++ingredientCount;
	let div = document.createElement('DIV');
	div.className = "ingredient";

	let amount_span = document.createElement('input');
	amount_span.type = "text";
	amount_span.className = "ingredient-amount";
	amount_span.setAttribute("value",amount);

	let name_span = document.createElement('input');
	name_span.type = "text";
	name_span.className = "ingredient-name";
	name_span.value = name;

	let delete_btn = document.createElement('BUTTON');
	delete_btn.innerHTML = "Delete";
	delete_btn.setAttribute('onclick', 'this.parentElement.remove(); --ingredientCount;');
	div.appendChild(amount_span);
	div.innerHTML += " ";
	div.appendChild(name_span);
	div.appendChild(delete_btn);

	$('#ingredients').append(div);
	$('#add-ingredient-name-input').val("");
	$('#add-ingredient-amount-input').val("");
}

$('#add-ingredient-button').on('click', () => {
	if($('#add-ingredient-name-input').val() == "" || $('#add-ingredient-amount-input').val() == "")
		return;
	++ingredientCount;
	var new_amount = $('#add-ingredient-amount-input').val();
	var new_name = $('#add-ingredient-name-input').val();
	addIngredient(new_amount, new_name)
	
});

let stepCount = 1;
function resetStepLabels()
{
	let stepsDiv = document.getElementById('steps');
	var childDivs = stepsDiv.getElementsByTagName('div');
	for(let i = 0; i < childDivs.length; ++i)
	{
		let label = childDivs[i].getElementsByTagName('label');
	 	label[0].innerHTML = "Step "+(i+1);
	}
}

function addStepWithContent(content)
{
	let div = document.createElement('DIV');
	div.className = "step";

	let label = document.createElement('LABEL');
	label.className = "step-number";
	label.innerHTML = "Step "+stepCount;

	let textarea = document.createElement('TEXTAREA');
	textarea.className = "step-content";
	textarea.value = content;

	let delete_btn = document.createElement('BUTTON');
	delete_btn.innerHTML = "Delete";
	delete_btn.className = "delete-btn";
	delete_btn.setAttribute('onclick', 'this.parentElement.remove(); --stepCount; resetStepLabels()');

	++stepCount;

	div.appendChild(label);
	div.appendChild(delete_btn);
	div.innerHTML += "<br />";
	div.appendChild(textarea);

	$('#steps').append(div);
}

function addStep()
{
	let div = document.createElement('DIV');
	div.className = "step";

	let label = document.createElement('LABEL');
	label.className = "step-number";
	label.innerHTML = "Step "+stepCount;

	let textarea = document.createElement('TEXTAREA');
	textarea.className = "step-content";

	let delete_btn = document.createElement('BUTTON');
	delete_btn.className = "delete-btn";
	delete_btn.innerHTML = "Delete";
	delete_btn.setAttribute('onclick', 'this.parentElement.remove(); --stepCount; resetStepLabels()');

	++stepCount;

	div.appendChild(label);
	div.innerHTML += "<br />";
	div.appendChild(textarea);
	div.appendChild(delete_btn);
	$('#steps').append(div);
}

$('#add-step-button').on('click', addStep);



$('#submit').on('click', () => {
	let rname = $('#name-input').val();
	let rdescription = $('#description-input').val();
	let ramounts = "";
	let ritems = "";
	let rsteps = "";
	let rid = $('#id').val();

	// Get all the ingredients and amounts
	let ingr = $('#ingredients').children();
	let spans = ingr.find('input');
	for(let i = 0; i < spans.length; ++i)
	{
		if(i < spans.length-2){
			if(i % 2 == 0)
				ramounts += spans[i].value + "\n";
			else
				ritems += spans[i].value + "\n";
		}
		else
		{
			if(i % 2 == 0)
				ramounts += spans[i].value;
			else
				ritems += spans[i].value;
		}
	}
	
	let stps = $('#steps');
	let textareas = stps.find('textarea');
	for(let i = 0; i < textareas.length; ++i)
	{
		if(i < textareas.length-1)
			rsteps += textareas[i].value + "\n";
		else
			rsteps += textareas[i].value;
	}
	
	$.post('/pages/php/add_recipe.php', {name: rname, description: rdescription, amounts: ramounts, items: ritems, steps: rsteps, id: rid}, (successful) =>{
		if(successful)
		{
			$('#add-recipe').html("<p>Recipe added!</p><a href='/?page=my_recipes'>Go to your recipes</a>");
		}
		else
		{
			$('#add-recipe').html("<h3>Sorry! Your recipe could not be added!</h3>");
		}
	});
});

$(document).ready(() =>{

	if(edit)
		$('#recipe-name').hide();
	else
		$('#recipe-description').hide();
	$('#recipe-ingredients').hide();
	$('#recipe-steps').hide();
	$('#recipe-submit').hide();

	$('#name-next').on('click',()=>{
		$('#recipe-name').hide();
		$('#recipe-description').show();
	});

	$('#description-back').on('click',()=>{
		$('#recipe-name').show();
		$('#recipe-description').hide();
	});

	$('#description-next').on('click',()=>{
		$('#recipe-description').hide();
		$('#recipe-ingredients').show();
	});

	$('#ingredients-back').on('click',()=>{
		$('#recipe-description').show();
		$('#recipe-ingredients').hide();
	});

	$('#ingredients-next').on('click',()=>{
		$('#recipe-ingredients').hide();
		$('#recipe-steps').show();
	});

	$('#steps-back').on('click',()=>{
		$('#recipe-ingredients').show();
		$('#recipe-steps').hide();
	});

	$('#steps-next').on('click',()=>{
		$('#recipe-steps').hide();
		$('#recipe-submit').show();
	});

	if(!edit)
	{
		for(let i = 0; i < 3; ++i)
		addStep();
	}
});
