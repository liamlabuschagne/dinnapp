$(document).ready(()=>{
	$('#btn').on('click', () => {
		if($('#email').val().length == 0)
			return;
		$.post('/pages/php/forgot_password.php', {email: $('#email').val()}, (data) =>{
			if(data)
				$('#forgot-password').html("<h1>Thanks!</h1><p>Now check your email for further instructions.</p>");
			else
				$('#forgot-password').html("<h1>Oops!</h1><p>We couldn't send you instructions, please try again.</p>");
		});
	});
});