<div id="forgot-password">
	<h1>Forgot your password?</h1>
	<p>Don't worry, we've got you covered. Just enter your email address here and we'll send you instructions on how
	to reset your password!</p>
	<label>Your email: </label><input type="email" id="email">
	<button id="btn">Go</button>
</div>
<script src="/pages/js/forgot_password.js"></script>