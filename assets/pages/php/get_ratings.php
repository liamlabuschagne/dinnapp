<?php
	include '../../mysqli_connect.php';
	$recipe_id = mysqli_real_escape_string($dbc,$_POST['recipe_id']);

	// Show previous ratings
	$sql = "SELECT * FROM ratings WHERE recipe_id = '$recipe_id' ORDER BY id DESC LIMIT 5";
	$result = mysqli_query($dbc,$sql);
	$ratings_count = mysqli_num_rows($result);
	if($ratings_count > 0)
	{
		while($row = mysqli_fetch_assoc($result))
		{
			echo "<h2>Ratings: ".$ratings_count."</h2>";
			echo "<div class='rating'>";
			echo "<h3 class='rating-name'>".$row['user_name']." rated: ".$row['rating']."/10</h3>";
			if($row['title'] != "")
				echo "<div class='comment'><i><h4 class='rating-title'>".$row['title']."</h4></i>";
			if($row['comment'] != "")
				echo "<p class='rating-comment'>".$row['comment']."</p></div>";
			echo "<p class='rating-date'>Date posted: ".$row['date']."</p>";
			echo "<br><a class='rating-report' href='/?page=report&ct=Rating&cid=".$row['id']."&ruid=".$row['user_id']."' target='_blank'>Report this rating</a>";
		}
	}
	else
	{
		echo "<p>No ratings yet.</p>";
	}
?>