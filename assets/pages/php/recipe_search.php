<?php
	include '../../mysqli_connect.php';
	
	// normal keyword search
	if(isset($_POST['searchString']))
	{
		// split search string into an array of words
		$searchArray = explode(" ",mysqli_real_escape_string($dbc,$_POST['searchString']));
		//initalise sql query variable
		$sql = 'SELECT id, recipe_name,description,avg_rating  FROM recipes WHERE ';
		// append first keyword
		$currentSearchString = $searchArray[0];
		$sql .= "(recipe_name LIKE '%$currentSearchString%' OR description LIKE '%$currentSearchString%' OR items LIKE '%$currentSearchString%') ";
		//append other keywords
		for($i = 1 ; $i < count($searchArray) ; $i++){
			$currentSearchString = $searchArray[$i];
			$sql .= "AND (recipe_name LIKE '%$currentSearchString%' OR description LIKE '%$currentSearchString%' OR items LIKE '%$currentSearchString%') ";

		}
		// add limit to end
		$sql .= "ORDER BY avg_rating DESC LIMIT 10";
		if(isset($_POST['offset'])){
			$offset = mysqli_real_escape_string($dbc,$_POST['offset']);
			$sql .= " OFFSET " . $offset;
		}
		$result = mysqli_query($dbc, $sql);
		$num_results = mysqli_num_rows($result);
		// fetch data and echo it out
		while($row = mysqli_fetch_row($result))
		{
			echo "<a href='/?page=view_recipe&id=".$row[0]."'><div class='recipe-result'><h2>" . $row[1] . "";
			// echo first part of description
			echo " [Rated ". round($row[3]) ."/10]</h2><p>" . substr($row[2],0,100) . "...</p></div></a>";
		}
		// echo how many rows there are in an input field
		echo "<input type='hidden' id='num-rows' value='". $num_results  . "'>";
		
	}
	// random search
	if(isset($_POST['random'])){
		// get how many rows in the table
		$sql = 'SELECT COUNT(id) FROM recipes';
		$result = mysqli_query($dbc, $sql);
		$row = mysqli_fetch_row($result);
		$data = $row[0] - 1;
		// set up sql query
		
		// get random number
		$rand = mt_rand(0,$data);
		// get 1 random result from the database
		$sql = 'SELECT id, recipe_name,description,avg_rating FROM recipes LIMIT 1 OFFSET ' . $rand;
		$result = mysqli_query($dbc, $sql);
		$row = mysqli_fetch_row($result);
		// show result
		echo "<a href='/?page=view_recipe&id=".$row[0]."'><div class='recipe-result'><h2>" . $row[1];
		// echo first part of description
		echo " [Rated ". round($row[3]) ."/10]</h2><p>" . substr($row[2],0,100) . "...</p></div></a>";
		
	}
?>