<?php
	include '../../mysqli_connect.php';
	// All data has been entered, continue

	// Create variables for all the data, at the same time strip them.
	$username = mysqli_real_escape_string($dbc,$_POST['username']);
	$password = mysqli_real_escape_string($dbc,$_POST['password']);
	// Chech the username is valid and get the hashed password
	$sql = "SELECT password,permissions,id,active FROM users WHERE username = '$username' AND google_id = -1";
	$result = mysqli_query($dbc,$sql);

	// Check if the username is valid
	if($result != false)
	{
		// Get the hashed password from the database
		$row = mysqli_fetch_row($result);
		$hashed_password = $row[0];
		// Check if the password is correct
		if(password_verify($password, $hashed_password))
		{
			// Log the user in
			$_SESSION['username'] = $username;
			echo "Logged in";
			$_SESSION["permissions"] = $row[1];
			$_SESSION["id"] = $row[2];

			$sql = "SELECT id FROM blocked_users WHERE user_id = $row[2] ";
			$result = mysqli_query($dbc,$sql);
			if(mysqli_num_rows($result) > 0){
				$_SESSION["blocked"] = true;
			} else {
				$_SESSION["blocked"] = false;
			}
			$_SESSION['active'] = $row[3];
		}
		else
		{
			// Tell the user that the password is incorrect
			echo "Incorrect username or password.";
		}
	}
?>