<?php
	include '../../mysqli_connect.php';
	$ct = mysqli_real_escape_string($dbc,$_POST['ct']); // Content type
	$cid = mysqli_real_escape_string($dbc,$_POST['cid']); // Content id
	$ruid = mysqli_real_escape_string($dbc,$_POST['ruid']); // User whos content was reported
	$rid = mysqli_real_escape_string($dbc,$_POST['rid']); // The user who made the report
	$reason = mysqli_real_escape_string($dbc,$_POST['reason']); // Reason for reporting

	$sql = "INSERT INTO reports
			(reported_content_id,reporter_user_id,reported_user_id,content_type,reason)
			VALUES
			($cid,$rid,$ruid,'$ct','$reason')";
	$result = mysqli_query($dbc,$sql);
	if($result)
		echo "Thanks! We will review this report A.S.A.P";
	else
		echo "Sorry, we couldn't submit this report please try again.";
?>