<h1>Please activate your account</h1>
<p>
	Please activate your account by following the instructions sent to you through your email, or <a href='/?page=logout'>keep browsing as a guest</a>. If you have not recieved an email please <a href="/?page=resend_email">click here</a> to resend it.
</p>