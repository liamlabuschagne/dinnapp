<script src="/pages/js/recipe_search.js"></script>
<link rel="stylesheet" type="text/css" href="/pages/css/recipe_search_mobile.css" media="only screen and (max-width:600px)">
<link rel="stylesheet" type="text/css" href="/pages/css/recipe_search_wide.css" media="only screen and (min-width:601px)">

	
<div id="search-display">
	<div id="keyword-search">
		<h1>Search for recipes</h1>
		<p>Type your keywords / ingredients. The results will pop up automatically.</p>
		<input type="text" id="input"><!-- search by key item, like in add recipie-->
	</div>
	<div id="random-search">
		<p> Or try our <button id="random_results">Random Search</button><!-- You press the button to get random recipies--></p>
	</div>
</div>

<div id="results"></div>
<!-- output div for recipies-->
<div id="page-buttons">
<button id="prev-button" disabled="true"><< prev</button><button id="next-button" disabled="true">next >></button>
</div>