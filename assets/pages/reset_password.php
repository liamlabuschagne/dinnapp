<?php
	$email = $_GET['email'];
	$hash = $_GET['hash'];
	echo "<input id='email' type='hidden' value='$email'>";
	echo "<input id='hash' type='hidden' value='$hash'>";
?>
<div id="reset-password">
	<h1>Reset Password</h1>
	<div id="errors"></div>
	<p>Type your new password below to reset it.</p>
	<label>New Password: </label><input type="password" id="password"><br />
	<label>New Password repeated:</label><input type="password" id="password-repeat">
	<button id="btn">Reset my password</button>
</div>
<script src="/pages/js/reset_password.js"></script>