<?php	
	// If they're logged in get their username
	if(isset($_SESSION['username']))
	{
		$username = $_SESSION['username'];
		if(isset($_SESSION['permissions']))
		{
			header('Location: /admin/?page=panel');
		}
		else
		{
			// Get their permissions
			$sql = "SELECT permissions FROM users WHERE username = '$username'";
			$result = mysqli_query($dbc,$sql);
			$permissions = mysqli_fetch_row($result)[0];
			if($permissions == "Administrator" || $permissions == "Moderator")
			{
				$_SESSION['permissions'] = $permissions;
				header("Location: /admin/?page=panel");
			}
			else
				echo "Insufficient privileges";
		}	
	}
	else
	{
		echo "Please log in.";
	}

?>