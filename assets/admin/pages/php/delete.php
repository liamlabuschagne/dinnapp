<?php
	include '../../../mysqli_connect.php';

	if(isset($_SESSION['permissions']))
	{
		$cid = mysqli_real_escape_string($dbc,$_GET['cid']);

		if(isset($_GET['rid']))
			$rid = mysqli_real_escape_string($dbc,$_GET['rid']);
		else
			$rid = false;

		$ct = mysqli_real_escape_string($dbc,$_GET['ct']);

		$sql = "DELETE FROM $ct WHERE id = $cid";
		echo $sql;

		if(mysqli_query($dbc,$sql) || $_GET['ct'] == "Report" && $rid != false)
		{
			$sql = "DELETE FROM reports WHERE id = $rid";
			mysqli_query($dbc,$sql);
			unset($_GET['ct']);
			unset($_GET['cid']);
			unset($_GET['rid']);
			header("Location: /admin/?page=panel");
		}
		else
		{
			echo "Unable to execute deletion";
		}
	}
	else 
	{
		header("Location: /");
	}
?>