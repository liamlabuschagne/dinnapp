<?php
	/* 	Include this file on top of every page in the website.
	 	It connects to the database and starts the session.
	 	Comment this line out if using xampp for testing, but always remeber to uncomment
	 	when pushing to BitBucket!
	*/
	include 'mysqli_connect.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php
		// This script will insert all the required meta data.
		// It include specific title and descriptions based on URL get variables.
		include 'head.php';
	?>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="/pages/js/default.js"></script>
	<link rel="stylesheet" type="text/css" media="only screen and (min-width:601px)" href="/pages/css/default_wide.css" >
	<link rel="stylesheet" type="text/css" media="only screen and (max-width:600px)" href="/pages/css/default_mobile.css" >
</head>
<body>
	<header>
		<img src="assets/images/dinnappbanner.png" id="banner-image" />
		<nav id="nav">
			<div class="nav-colomn">
				<ul>
					<li><a href="/?page=home">Home</a></li>
					<?php 
						// display login buttons etc
						if(isset($_SESSION['username']))
						{
							echo '<li><a href="/?page=add_recipe">Add Recipe</a></li>';
							echo '<li><a href="/?page=my_recipes">My Recipes</a></li>';
							echo "<li><a href='/?page=profile'>Logged in as " . $_SESSION['username'] . "</a></li>";
							echo "<li><a href='/?page=logout'>Logout</a></li>";
						}
						else
						{
							echo "<li><a href='/?page=login'>Login</a></li> ";
							echo "<li><a href='/?page=register'>Register</a></li>";
						}
					?>
				</ul>
			</div>
			<div class="nav-colomn">
				<ul>

					<?php
					if(isset($_SESSION['username'])){
						echo "<li><a href='/?page=favourites'>My Favourites</a></li>";
					}
					if(isset($_SESSION['permissions'])){
						if($_SESSION['permissions'] == "Moderator" || $_SESSION['permissions'] == "Administrator")
							echo "<li> <a href='/admin/'>Moderator/admin page</a></li>";
					}
					?>
					
					<li><a href="/?page=recipe_search">Search Recipes</a></li>
					<li><a href="/?page=about">About</a></li>
					<li><a href="/?page=contact">Contact</a></li>
					
				</ul>
			</div>
		</nav>
	</header>
	<main>
		<?php
			// Check the URL get variables for 'page', if that page exits include it. 
			if(isset($_GET["page"]) && file_exists("pages/" . $_GET["page"] . ".php")){
				include "pages/" . $_GET["page"] . ".php";
			}
			else {
				include "pages/home.php";
			}
		?>
	</main>
	<footer>
		Created by <a href="http://liamsblog.tk">Liam Labuschagne</a>and
		<a href="http://ngin.cf">Bert Downs</a>
	</footer>
</body>
</html>